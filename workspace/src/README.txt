Please distribute this file with the Valkyrie library

Valkyrie is a general purpose library aimed at developing 
games in FreePascal. More about Valkyrie can be read at
http://valkyrie.chaosforge.org/

Documentation is available on the site.

The current version is still in early beta, and many of the data
structures may be changed. Instead of trying to use these libraries
I would prefer people to list me requests on what should be added.

WARNING - There are many places that lack proper error-checking.

This library is distributed under the terms of the GNU LGPL license:
http://www.gnu.org/copyleft/lesser.html

Copyright (C) 2005-2011 by Kornel Kisielewicz