{$I valkyrie.inc}
program valkyrie_path;
uses
  vsystems, vdebug, vtoutput, vtinput, voutput, vmath, vinput, vutil, vrltools, vpath, vdungen,
  Classes, SysUtils;

const
  MAP_SIZE_X = 80;
  MAP_SIZE_Y = 50;


type TCellData = record
  Ascii : Char;
  Color : Byte;
end;

const CellData : array[0..3] of TCellData = (
  ( Ascii : '.'; Color : LightGray; ),
  ( Ascii : '#'; Color : DarkGray; ),
  ( Ascii : '+'; Color : Brown; ),
  ( Ascii : '='; Color : Yellow; )
);

type

{ TMap }

TMap = class( IPathQuery )
  Cells : array[1..MAP_SIZE_X,1..MAP_SIZE_Y] of Byte;
  Area  : TArea;
  Path  : TPathfinder;
  DrawPath   : Boolean;
  DrawOpen   : Boolean;
  DrawClosed : Boolean;
  constructor Create;
  procedure Draw;
  function MoveCost( const Start, Stop : TCoord2D ) : Single;
  function CostEstimate( const Start, Stop : TCoord2D ) : Single;
  function passableCoord( const Coord : TCoord2D ) : Boolean;
  function RanFloor : TCoord2D;
  destructor Destroy;
end;

{ TMap }

constructor TMap.Create;
begin
  DrawOpen   := False;
  DrawClosed := False;
  DrawPath   := False;
  Area.Create( NewCoord2D(1,1), NewCoord2D( MAP_SIZE_X, MAP_SIZE_Y ) );
  Path := TPathfinder.Create( Self );
end;

procedure TMap.Draw;
var x, y : Byte;
    Node : TAStarNode;
    c    : DWord;
begin
  for x := 1 to MAP_SIZE_X do
    for y := 1 to MAP_SIZE_Y do
      Output.DrawChar(x,y,CellData[Cells[x,y]].Color,CellData[Cells[x,y]].Ascii);

  if DrawOpen and (Path.FOpen.Size > 0) then
  for c := 0 to Path.FOpen.Size-1 do
    with Path.FOpen[c] do
      Output.DrawChar(Coord.x,Coord.y,Green,'*');

  if DrawClosed and (Path.FClosed.Size > 0) then
  for c := 0 to Path.FClosed.Size-1 do
    with Path.FClosed[c] do
      Output.DrawChar(Coord.x,Coord.y,Red,'*');

  if DrawPath and Path.Found then
  begin
    Node := Path.Start;
    while Node.Child <> nil do
    begin
      Output.DrawChar(Node.Coord.x,Node.Coord.y,LightCyan,'*');
      Node := Node.Child;
    end;
  end;
end;

function TMap.MoveCost(const Start, Stop: TCoord2D): Single;
var Diff : TCoord2D;
begin
  if Cells[Stop.x,Stop.y] = 1 then Exit( 1000 );
  Diff := Start - Stop;
  if Diff.x * Diff.y = 0
     then Exit(1.0)
     else Exit(1.3);
end;

function TMap.CostEstimate(const Start, Stop: TCoord2D): Single;
var Dist : TCoord2D;
begin
  Exit( RealDistance(Start,Stop) )
end;

function TMap.passableCoord(const Coord: TCoord2D): Boolean;
begin
  Exit( Cells[Coord.X, Coord.Y] <> 1 );
end;

function TMap.RanFloor : TCoord2D;
begin
  repeat
    RanFloor := Area.RandomInnerCoord;
  until Cells[ RanFloor.x, RanFloor.y ] = 0;
end;

destructor TMap.Destroy;
begin
  FreeAndNil( Path );
end;

var Map : TMap;

type

{ TBuilder }

TBuilder = class( TDungeonBuilder )
  constructor Create; reintroduce;
  function GetCell( const aWhere : TCoord2D ) : Byte; override;
  procedure PutCell( const aWhere : TCoord2D; const aWhat : Byte ); override;
  function isEmpty( const coord : TCoord2D; EmptyFlags : TFlags32 = []) : Boolean; override;
end;

{ TBuilder }

constructor TBuilder.Create;
begin
  inherited Create( MAP_SIZE_X, MAP_SIZE_Y );
  SetFloorCell( 0 );
  SetWallCell( 1 );
  SetDoorCell( 2 );
end;

function TBuilder.GetCell(const aWhere: TCoord2D): Byte;
begin
  Exit( Map.Cells[aWhere.X, aWhere.Y] );
end;

procedure TBuilder.PutCell(const aWhere: TCoord2D; const aWhat: Byte);
begin
  Map.Cells[aWhere.X, aWhere.Y] := aWhat;
end;

function TBuilder.isEmpty(const coord: TCoord2D; EmptyFlags: TFlags32 ): Boolean;
begin
  Exit( True );
end;

var Builder        : TBuilder;
    Key            : Byte;
    Cur,Start,Stop : TCoord2D;

begin
  Output  := TTextModeOutput.Create( MAP_SIZE_X, MAP_SIZE_Y );
  Input   := TTextModeInput.Create();
  Map     := TMap.Create;
  Builder := TBuilder.Create;
  Cur.Create(2,2);
  Output.MoveCursor(Cur.X,Cur.Y);
  Builder.RestoreWalls(1);
  Builder.MazeDungeon(0,1,2,300,4,8);
  Start := NewCoord2D(2,2);
  Stop  := NewCoord2D(MAP_SIZE_X-1,MAP_SIZE_Y-1);
  repeat
    Map.Draw;
    Output.DrawChar( Start.x, Start.y, Yellow, '*' );
    Output.DrawChar( Stop.x,  Stop.y,  LightBlue, '*' );
    Output.DrawString( 3, 1, LightGreen, ' '+IntToStr( AStarCount )+' / '+IntToStr( Map.Path.MaxNodes ) );
    Output.Update;
    Key := Input.GetKey;
    case Key of
      Ord('0')   : Map.Cells[Cur.x,Cur.y] := 0;
      Ord('1')   : Map.Cells[Cur.x,Cur.y] := 1;
      Ord('2')   : Map.Cells[Cur.x,Cur.y] := 2;
      Ord('3')   : Map.Cells[Cur.x,Cur.y] := 3;
      Ord('s')   : Start := Cur;
      Ord('e')   : Stop  := Cur;
      Ord('p')   : begin Map.Path.Run( Start, Stop, 30,100 ); Map.DrawPath := True; end;
      Ord('P')   : Map.DrawPath := False;
      Ord('o')   : Map.DrawOpen   := not Map.DrawOpen;
      Ord('c')   : Map.DrawClosed := not Map.DrawClosed;
      VKEY_LEFT  : Cur.x := Cur.x - 1;
      VKEY_RIGHT : Cur.x := Cur.x + 1;
      VKEY_UP    : Cur.y := Cur.y - 1;
      VKEY_DOWN  : Cur.y := Cur.y + 1;
      Ord('r')   :
        begin
          Builder.Fill(0);
          Builder.RestoreWalls(1);
          Builder.MazeDungeon(0,1,Random(2)+2,300,4,8);
          Map.Path.Run( Start, Stop, 30 );
          Map.DrawPath := True;
        end;
    end;
    Map.Area.Clamp(Cur);
    Output.MoveCursor(Cur.X,Cur.Y);
  until Key = VKEY_ESCAPE;
  FreeAndNil( Map );
  FreeAndNil( Builder );
end.

